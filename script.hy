#!/usr/local/bin/hy
(import os re requests)
(import [xml.dom [minidom]])
(print __file__)
(setv +channel-file-path+ (os.path.join (os.path.dirname __file__) "channels")
      +rss-url-prefix+ "https://www.youtube.com/feeds/videos.xml?channel_id=")
(defn channels-list []
  "Returns a list of channel RSS feed URLs"
  (list (map (fn [line] (+ +rss-url-prefix+
                           (get (re.match "^.{24}" line) 0)))
             (with [f (open +channel-file-path+)]
               (.readlines f)))))
(defn read-channel [url]
  (. (requests.get url) text))
(defn remove-tag [raw]
  (.group (re.match f"<[a-z]*>(.*)</[a-z]*>" raw) 1))
(defn links [dom]
  (list (rest (rest (dom.documentElement.getElementsByTagName "link")))))
(defn link-titles [dom]
  (list (rest (dom.documentElement.getElementsByTagName "title"))))
(for [channel-url (channels-list)]
  (setv dom (minidom.parseString (read-channel channel-url)))
  (print (remove-tag (.toxml (get (dom.documentElement.getElementsByTagName "name")
                                  0))))
  (for [i (range (len (links dom)))]
    (print (+ " " (remove-tag (.toxml (get (link-titles dom) i)))))
    (print (+ "  " (.group
                     (re.match r"<link href=\"(.*?)\" rel=\"alternate\"/>"
                               (.toxml (get (links dom) i)))
                     1)))))


