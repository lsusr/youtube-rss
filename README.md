# Documentation
This is a lightweight script that reads the RSS feeds or your favorite YouTube channels. It's intended to be used with `youtube-dl` as an alternative to YouTube's home page.
## Instructions for use
1. Create an untracked file named `channels`. Put the channel ids of your favorite YouTube channels in the `channels` file, one per line.
2. Install `hy` and any other necessary Python3 libraries.
3. Run the script `./script.hy`
4. Install `youtube-dl`.
5. Cut and paste the URLs of the videos you want to download into `youtube-dl`'s arguments.
